# Challenge 12 - “Number Rendering” (Intermediate)

## Problem:
Given any whole integer number, your task this week is simply to output the number to the console in the traditional LCD “calculator font”. You shouldn’t use any external libraries to achieve this.

### 7 Points are awarded for a working solution
### 3 Further points are awarded for a solution in under 50 lines of code

<br />

## Example:
Given the number 123456789, the expected output might look like this:
```text
  _  _     _  _  _  _  _
| _| _||_||_ |_   ||_||_|
||_  _|  | _||_|  ||_| _| 

```

