const topChars = ['  ', ' _  ', '_  ', '    ', ' _  ', ' _  ', '_  ', ' _  ', ' _  ',];
const centerChars = ['| ', ' _| ', '_| ', '|_| ', '|_  ', '|_  ', ' | ', '|_| ', '|_| ',];
const bottomChars = ['| ', '|_  ', '_| ', '  | ', ' _| ', '|_| ', ' | ', '|_| ', ' _| ',];

const renderNumbers = num => {
    let top = '';
    let center = '';
    let bottom = '';

    String(num).split('').forEach(char => {
        if (char === '0') { return }
        top += topChars[Number(char) - 1];
        center += centerChars[Number(char) - 1];
        bottom += bottomChars[Number(char) - 1];
    });
    console.log(`${top}\n${center}\n${bottom}`);
};

renderNumbers(123654789);
